import { DefinePlugin } from 'webpack';
import * as path from 'path';
import * as ExtractTextPlugin from 'extract-text-webpack-plugin';
import * as bourbon from 'bourbon';
import * as ModernizrWebpackPlugin from 'modernizr-webpack-plugin';

const colors: IColors = {
  primaryColor: '#262463',
  accentColor: '#DA1188',
};

export default {
  entry: {
    main: path.join(__dirname, 'src', 'main', 'index.tsx'),
  },
  output: {
    path: path.join(__dirname, 'public', 'build'),
    publicPath: '/build/',
    filename: 'bundle.js',
  },
  externals: {
    'react/addons': true,
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.scss'],
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        enforce: 'pre',
        loader: 'source-map-loader',
      },
      {
        test: /\.tsx?$/,
        exclude: /node_modules/,
        loader: 'awesome-typescript-loader',
      },
      {
        test: /\.s?css$/,
        loader: ExtractTextPlugin.extract([
          {
            loader: 'css-loader',
          }, {
            loader: 'sass-loader',
            options: {
              includePaths: [bourbon.includePaths],
              data: `
                $primaryColor: ${colors.primaryColor};
                $accentColor: ${colors.accentColor};
              `,
            },
          },
        ]),
      },
    ],
  },
  devServer: {
    contentBase: path.join(__dirname, 'public'),
  },
  plugins: [
    new ExtractTextPlugin({
      filename: 'styles.css',
      allChunks: true,
    }),
    new DefinePlugin({
      APP_COLORS: JSON.stringify(colors),
    }),
    new ModernizrWebpackPlugin({
      'feature-detects': [
        'touchevents',
      ],
    }),
  ],
};
