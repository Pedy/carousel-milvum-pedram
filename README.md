# README #


## Introduction ##
For this assignment, one of the carousels of the design file had to be implemented. I chose the first
carousel to implement because I saw that one as the base carousel and the other ones are simple extensions
of the first one. Creating the carousel, I chose the following frameworks/libraries/tools/languages:
React written in TypeScript with use of Webpack to transpile and bundle to JavaScript. I used SASS for
styling which compiles to a single CSS file using Webpack.

While designing the carousel, I used TypeScript for only two things while handling the rest in SASS.
Creating the HTML elements in React and Modernizr to register touch support. This way, by not handling
the transitions and animations in TypeScript, allows for the carousel to be as performant as possible.

There is furthermore some basic testing implemented using Karma, Mocha and Chai. A simple implementation
testing whether an element is being correctly rendered or not.


## React ##
I chose React because of its ease in use and the ability to quickly create the elements of the carousel.
Furthermore, due to the nature of React and usage of props in the Carousel component I have created,
This base carousel can be used for the other carousel designs as well. This gives high customizability
and low code repetition.


## TypeScript ##
I chose for TypeScript because it is more robust than JavaScript, but with the same capabilities. It
allows for easier debugging and code completion (with the right IDE). It is overall a better choice than
using JavaScript, although both can be used together may the need arise.


## Webpack ##
Webpack is easy to setup, continiously improving and has great support. It makes setting up and developing
a website much easier than other tools in my opinion and using WebpackDevServer, local development with
hot reloading allows for really quick development.


## SASS ##
I don't believe I need to justify the use of a preprocessor, but for me chosing SASS over other preprocessors,
it's mostly a matter of preference. I'm familiar with LESS as well which is another popular preprocessor, but
my preference goes to SASS because of minor syntax differences.


## Build ##
There are two different ways to run this project:
- Running the commands `npm install` and `npm start` which allows viewing the website on __localhost:8080__
- Copying the zip inside the public folder to a different folder, unzipping and then opening the __index.html__ file 
