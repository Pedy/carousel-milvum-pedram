const homeAssets: string[] = [];
const sociAssets: string[] = [];
const sociaAssets: string[] = [];
const destAssets: string[] = [];

for (let i = 1; i <= 5; i++) {
  homeAssets[i - 1] = `assets/home${i}.jpg`;
  sociAssets[i - 1] = `assets/soci${i}.png`;
  sociaAssets[i - 1] = `assets/socia${i}.png`;

  if (i === 5) { continue; } // dest5.png does not exist
  destAssets[i - 1] = `assets/dest${i}.png`;
}

export {
  homeAssets,
  sociAssets,
  sociaAssets,
  destAssets
}
