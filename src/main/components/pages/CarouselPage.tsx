import * as React from 'react';

import {
  Page,
  Carousel,
} from '../';

export default class CarouselPage extends React.Component<any, any> {
  public render() {
    return (
      <Page className="carousel-page">
        <Carousel />
      </Page>
    );
  }
}
