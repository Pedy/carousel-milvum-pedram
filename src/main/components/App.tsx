import * as React from 'react';

import { CarouselPage } from './';

export default class App extends React.Component<any, undefined> {
  public render() {
    return (
      <CarouselPage />
    );
  }
}
