import * as React from 'react';
import * as classnames from 'classnames';

export default ({ className, children }: React.Props<any> & React.HTMLProps<any>) => (
  <div className={classnames('page', { [className as string]: !!className })}>
    {children}
  </div>
);
