import * as React from 'react';
import * as classNames from 'classnames';

import { homeAssets } from '../assets';

export default () => (
  <div
    className={classNames(
      'carousel',
      {
        'has-touch': (window as any).Modernizr && (window as any).Modernizr.touchevents,
      },
    )}
  >
    {
      /* Activators */
      homeAssets.map((asset, index) => (
        <input
          key={index}
          id={index.toString()}
          className="carousel__activator"
          type="radio"
          name="bullet"
          defaultChecked={index === 0}
        />
      ))
    }

    {
      /* Slides */
      homeAssets.map((asset, index) => (
        <div
          key={index}
          className="carousel__slide"
          style={{
            backgroundImage: `url(${asset}`,
          }}
        >
          <div className="slide__meta">Meta content for slide {index + 1}</div>
          {
            index > 0 && (
              <label className="carousel__arrow carousel__arrow--left" htmlFor={index - 1 + ''}>
                <i className="material-icons">keyboard_arrow_left</i>
              </label>
            )
          }
          {
            index < homeAssets.length - 1 && (
              <label className="carousel__arrow carousel__arrow--right" htmlFor={index + 1 + ''}>
                <i className="material-icons">keyboard_arrow_right</i>
              </label>
            )
          }
        </div>
      ))
    }

    {/* Bullets */}
    <div className="carousel__bullets">
      {
        homeAssets.map((asset, index) => (
          <label key={index} className="carousel__bullet" htmlFor={index.toString()} />
        ))
      }
    </div>

    {/* Some tool */}
    <div className="carousel__tool" />
  </div >
);
