import App from './App';

// Pages
import CarouselPage from './pages/CarouselPage';

import Page from './Page';
import Carousel from './Carousel';

export {
  App,
  /* ==================== */
  CarouselPage,
  /* ==================== */
  Page,
  Carousel,
}
