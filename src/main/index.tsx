import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { App } from './components/';
import './styles/main.scss';

// Required imports for React MDL
import 'react-mdl/extra/material.css';
import 'react-mdl/extra/material.js';

ReactDOM.render(<App />, document.getElementById('application'));
