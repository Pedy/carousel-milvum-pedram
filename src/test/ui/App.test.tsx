import { expect } from 'chai';
import { shallow, ShallowWrapper } from 'enzyme';
import * as React from 'react';
import { Provider } from 'react-redux';
import { MuiThemeProvider } from 'material-ui/styles';

import { App, CarouselPage } from '../../main/components/';

let wrapper: ShallowWrapper<any, any>;

describe('<App />', () => {
  before(() => {
    wrapper = shallow(<App />);
  });

  it('contains a <CarouselPage /> component', () => {
    const child = wrapper.find(CarouselPage);

    expect(child, 'Missing component').to.have.length(1);
  });
});
