/* tslint:disable no-reference */
/// <reference path="../../node_modules/immutable/contrib/cursor/index.d.ts" />

declare interface IColors {
  primaryColor: string;
  accentColor: string;
}

declare const APP_COLORS: IColors;

declare interface IAuthenticationState {
  isLoading: boolean | null;
  errorMessage: string | null;
  qrCode: string | null;
  sessionJwt: string | null;
}

interface IRecordState<S> extends Immutable.Map<string, any> {
  toObject(): S;
}

declare interface IAppState {
  authentication: IRecordState<IAuthenticationState>;
}
