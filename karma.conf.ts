import webpackConfig from './webpack.config';
import { Builder } from 'selenium-webdriver';

export default (config: any) => {
  config.set({
    frameworks: ['mocha'],

    files: [
      { pattern: 'src/test/index.test.ts' },
    ],

    preprocessors: {
      'src/**/*.{ts,tsx}': ['webpack', 'sourcemap'],
    },

    reporters: ['mocha', 'coverage'],

    port: 9876,

    colors: true,

    autoWatch: false,

    browsers: ['Chrome'],

    customLaunchers: {
      swd_ie: {
        base: 'SeleniumWebdriver',
        browserName: 'IE',
        getDriver: () => {
          const driver = new Builder().forBrowser('ie').build();

          return driver;
        },
      },
      swd_chrome: {
        base: 'SeleniumWebdriver',
        browserName: 'Chrome',
        getDriver: () => {
          const driver = new Builder().forBrowser('chrome').build();

          return driver;
        },
      },
    },

    singleRun: true,

    concurrency: Infinity,

    logLevel: config.LOG_INFO,

    mime: {
      'text/x-typescript': ['ts', 'tsx'],
    },

    mochaReporter: {
      showDiff: true,
    },

    coverageReporter: {
      reporters: [
        { type: 'text' },
        { type: 'text-summary' },
      ],
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      quiet: true,
      stats: {
        colors: true,
      },
    },
  });
};
